//
//  IssueModelService.swift
//  CivocracyIos
//
//  Created by Božidar Kokot on 17/07/2017.
//  Copyright © 2017 letsense. All rights reserved.
//

import Foundation
class IssueModelService{
    
    
    func getIssuesList(callBack:@escaping ([IssueDataModel]) -> Void){
        var issuesModel:[IssueDataModel] = []
        let restAPI = RestAPI()
        
        restAPI.get(request: RestAPI.issuesUrlRequest()){ (success, object) -> () in
            
            if(success){
                let outterDictionary = object as! NSDictionary
                
                for (key, value) in outterDictionary {
                    
                    if(key as! String == "issues"){
                        
                        
                        
                        
                        
                        let innerArray = value as! [Any]
                        
                        
                        for valueInIssueArray in innerArray{
                            
                            let innerDictionary = valueInIssueArray as! [String:Any]
                        
                        do {
                            let restaurant = try IssueDataModel(json: innerDictionary)
                            issuesModel.append(restaurant)
                        } catch let error {
                            print(error)
                        }
                     
                         }
                        
                        
                    }
                    
                }
                
                callBack(issuesModel)
                
            }else{
                
                print("failure")
            }
        }
        
        
    }
}
