//
//  IssueViewController.swift
//  CivocracyIos
//
//  Created by Božidar Kokot on 17/07/2017.
//  Copyright © 2017 letsense. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class IssueViewController:UIViewController{
    

    @IBOutlet weak var issueTableView: UITableView!
 
    private let issuePresenter = IssuePresenter(issueService: IssueModelService())
     var issueToDisplay = [IssueViewData]()
    
    
    
    override func viewWillAppear(_ animated:Bool) {
        
        super.viewWillAppear(animated)
        

        
        issueTableView.dataSource = self
        issueTableView.delegate = self
        issueTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        
        issuePresenter.attachView(view: self)
        
        issuePresenter.getIssues()
        
        
    }
    
   
    
    
}
//** extension of IssuesView protocol that is triggered from the presenter, and preforms UI updates
extension IssueViewController:IssueView{
    
    func setIssues(item: [IssueViewData]) {
        issueToDisplay = item
        
        DispatchQueue.main.async {
            self.issueTableView.reloadData()
        }
    }
    
   
}


/** TABLEVIEW DELEGATE METHODS **/
extension IssueViewController:UITableViewDataSource,UITableViewDelegate{
    
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return issueToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
 
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IssueCell", for: indexPath) as! IssueTableCell
        let itemViewData = issueToDisplay[indexPath.section]
        
        
        cell.followers.text = itemViewData.followers
        cell.issueTag.text = itemViewData.tag
        cell.issueSummary.text = itemViewData.summary
        cell.lastActive.text = itemViewData.date
        
        cell.backImg.sd_setImage(with: URL(string: itemViewData.issueImg), placeholderImage: UIImage(named: "placeholder.jpg"))
        cell.userImg.sd_setImage(with: URL(string: itemViewData.userImg), placeholderImage: UIImage(named: "placeholder.jpg"))

    
        
        
        return cell
    }
    
    
 
    
}


