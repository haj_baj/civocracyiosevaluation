//
//  Constants.swift
//  CivocracyIos
//
//  Created by Božidar Kokot on 17/07/2017.
//  Copyright © 2017 letsense. All rights reserved.
//

import Foundation
struct Constants {
    
    static let BASE_URL = "https://admin.civocracy.org/api/issues?filters[community]=384&filters[official]=1&order_by[date]=desc"
   static let BASE_IMG_URL = "http://res-1.cloudinary.com/civocracy/image/upload/"
    static let BASE_USER_IMG_URL = "http://res-1.cloudinary.com/civocracy/image/upload/users/"


}

