//
//  IssueDataModel.swift
//  CivocracyIos
//
//  Created by Božidar Kokot on 17/07/2017.
//  Copyright © 2017 letsense. All rights reserved.
//

import Foundation

struct IssueDataModel {
    let tag:String
    let summary:String
    let issueImg:String
    var date:String
    let followers:String
    let userImg:String
}
enum SerializationError: Error {
    case missing(String)
    case invalid(String, Any)
}

extension IssueDataModel {
    init(json: [String: Any]) throws {
        // Extract name
        guard let tag = json["tag"] as? String else {
            throw SerializationError.missing("tag")
        }
        guard let summary = json["summary"] as? String else {
            throw SerializationError.missing("summary")
        }
        guard let issueImg = json["image"] as? String else {
            throw SerializationError.missing("image")
        }
        guard let followers = json["followersNumber"] as? Int else {
            throw SerializationError.missing("followersNumber")
        }
        guard let date = json["date"] as? String else {
            throw SerializationError.missing("date")
        }
        guard let userId = json["user"] as? Int else {
            throw SerializationError.missing("user")
        }
        // Initialize properties
        self.tag = tag
        self.summary = summary
        self.issueImg = Constants.BASE_IMG_URL + issueImg
        self.followers = "+ " + String(followers) + " people"
        self.userImg = Constants.BASE_USER_IMG_URL + String(userId)
        self.date = date
        
       self.date = dateDiff(dateStr: date)

    
}
    
   private func dateDiff(dateStr:String) -> String {
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        
   
        let dateComponents = calendar.components([.weekOfMonth,.day,.hour,.minute,.second], from: startDate!, to: endDate!)
        
        let weeks = abs(Int32(dateComponents.weekOfMonth!))
        let days = abs(Int32(dateComponents.day!))
        let hours = abs(Int32(dateComponents.hour!))
        let min = abs(Int32(dateComponents.minute!))
        let sec = abs(Int32(dateComponents.second!))
        
        var timeAgo = ""
        
        if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) Seconds Ago"
            } else {
                timeAgo = "\(sec) Second Ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) Minutes Ago"
            } else {
                timeAgo = "\(min) Minute Ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) Hours Ago"
            } else {
                timeAgo = "\(hours) Hour Ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) Days Ago"
            } else {
                timeAgo = "\(days) Day Ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) Weeks Ago"
            } else {
                timeAgo = "\(weeks) Week Ago"
            }
        }
        
        return timeAgo;
    }
}



