//
//  IssuePresenter.swift
//  CivocracyIos
//
//  Created by Božidar Kokot on 17/07/2017.
//  Copyright © 2017 letsense. All rights reserved.
//

import Foundation
import Foundation

//View model structure representation of the model data
struct IssueViewData{
    let tag:String
    let summary:String
    let issueImg:String
    let date:String
    let followers:String
    let userImg:String
    
}

// Protocol that communicates with view controller
protocol IssueView: NSObjectProtocol {
    
    func setIssues(item: [IssueViewData])
    
   
    
}


class IssuePresenter {
    private let issueService:IssueModelService
    weak private var issueView : IssueView?
    
    init(issueService:IssueModelService){
        self.issueService = issueService
    }
    
    func attachView(view:IssueView){
        issueView = view
    }
    
    func detachView() {
        issueView = nil
    }
    
    
    // Called on viewDidLoad sends the list of issues to view
    func getIssues(){
        
        var issueItemViewData:[IssueViewData] = []
        
        issueService.getIssuesList{ item in
            
            for issue in item{
                issueItemViewData.append(IssueViewData(tag: issue.tag, summary: issue.summary, issueImg: issue.issueImg, date: issue.date, followers: issue.followers, userImg: issue.userImg))
            }
           
            
            self.issueView?.setIssues(item: issueItemViewData)
        }
        
        
     
        
    }
    

    
    
}
