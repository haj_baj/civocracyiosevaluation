//
//  IssueTableCell.swift
//  CivocracyIos
//
//  Created by Božidar Kokot on 17/07/2017.
//  Copyright © 2017 letsense. All rights reserved.
//

import Foundation
import UIKit
class IssueTableCell:UITableViewCell{
    
    
    @IBOutlet weak var backImg: UIImageView!
    
    @IBOutlet weak var lastActive: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var issueSummary: UILabel!
    @IBOutlet weak var issueTag: UILabel!
}
