//
//  RestAPI.swift
//  CivocracyIos
//
//  Created by Božidar Kokot on 17/07/2017.
//  Copyright © 2017 letsense. All rights reserved.
//


// Class responsible for calls to JSON REST
// The network calls go through NSURLSession which fires a callback with JSON in it
import Foundation
public class RestAPI{
    
    
    private func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    completion(true, json as AnyObject)
                } else {
                    completion(false, json as AnyObject)
                }
            }
            }.resume()
    }
    
    public func post(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "POST", completion: completion)
        
        
    }
    
    private func put(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "PUT", completion: completion)
    }
    
    public func get(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "GET", completion: completion)
    }
    
    
    class func issuesUrlRequest() -> NSMutableURLRequest {
        
        let urlString = Constants.BASE_URL
        
        let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }
    
   
    
    
}
